$(document).on('click', '.number-spinner button', function () {    
	var btn = $(this),
		oldValue = btn.closest('.number-spinner').find('input').val().trim(),
		newVal = 0;
	
	if (btn.attr('data-dir') == 'up') {
		newVal = parseInt(oldValue) + 1;
	} else {
	if (oldValue > 1) {
		newVal = parseInt(oldValue) - 1;
	} else {
		newVal = 1;
		}
	}
	btn.closest('.number-spinner').find('input').val(newVal);
});


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}


/* function myFunction() {

	var support;
	if (document.getElementById('support1').checked) {
  support = document.getElementById('support1').value;
	 }
	 else if(document.getElementById('support2').checked)
	 {
	 	support=document.getElementById('support2').value;
	 }

}

*/

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}